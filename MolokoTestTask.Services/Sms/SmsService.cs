﻿using System.IO;
using System.Net;
using System.Text;
using MolokoTestTask.Core.Infrastructure;

namespace MolokoTestTask.Services.Sms
{
    public class SmsService : ISmsService
    {
        #region Methods

        public string Send(string messagesXml)
        {
            try
            {
                var request = (HttpWebRequest) WebRequest.Create(Constants.SmsApiUrl);

                request.ContentType = "text/xml; encoding='utf-8'";
                request.Method = "POST";

                var bytes = Encoding.ASCII.GetBytes(messagesXml);
                request.ContentLength = bytes.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        return reader.ReadToEnd().Trim();
                    }
                }
            }
            catch (WebException ex)
            {
                return !(ex.Response is HttpWebResponse response)
                    ? HttpStatusCode.InternalServerError.ToString()
                    : response.StatusCode.ToString();
            }
        }

        #endregion
    }
}