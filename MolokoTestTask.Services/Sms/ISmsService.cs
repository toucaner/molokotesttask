﻿namespace MolokoTestTask.Services.Sms
{
    public interface ISmsService
    {
        #region Methods

        string Send(string messagesXml);

        #endregion
    }
}