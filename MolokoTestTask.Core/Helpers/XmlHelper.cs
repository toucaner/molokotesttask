﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using MolokoTestTask.Core.Infrastructure;
using MolokoTestTask.Core.Models;
using static System.Int32;

namespace MolokoTestTask.Core.Helpers
{
    public static class XmlHelper
    {
        #region Methods

        public static string GetRequestXml(IEnumerable<SmsRequestItem> messages)
        {
            var messagesDocument = new XDocument();

            var packageElement = new XElement("package");
            packageElement.Add(new XAttribute("login", Constants.Login));
            packageElement.Add(new XAttribute("password", Constants.Password));
            messagesDocument.Add(packageElement);

            var sendElement = new XElement("send");
            foreach (var message in messages)
            {
                var messageElement = new XElement("message", message.Message);
                messageElement.Add(new XAttribute("receiver", message.Receiver));
                if (!string.IsNullOrEmpty(message.Sender))
                    messageElement.Add(new XAttribute("sender", message.Sender));

                sendElement.Add(messageElement);
            }

            messagesDocument.Add(sendElement);

            return messagesDocument.ToString();
        }

        public static IEnumerable<SmsResponseItem> GetSmsResponseItems(string response)
        {
            var responseItems = new List<SmsResponseItem>();
            var document = new XmlDocument();
            document.LoadXml(response);

            var rootElement = document.DocumentElement;
            if (rootElement != null)
            {
                foreach (XmlElement node in rootElement)
                {
                    var responseItem = new SmsResponseItem();

                    foreach (XmlNode childnode in node.ChildNodes)
                    {
                        if (childnode.Name == "message")
                            responseItem.Message = childnode.InnerText;

                        var idAttribute = childnode.Attributes?.GetNamedItem("id");
                        if (idAttribute != null)
                            responseItem.Id = Parse(idAttribute.Value);
                        var serveridAttribute = childnode.Attributes?.GetNamedItem("server_id");
                        if (serveridAttribute != null)
                            responseItem.ServerId = Parse(serveridAttribute.Value);

                        foreach (XmlNode errornode in childnode.ChildNodes)
                            if (errornode.Name == "error")
                                responseItem.ErrorCode = errornode.InnerText;
                    }

                    responseItems.Add(responseItem);
                }
            }

            return responseItems;
        }

        #endregion
    }
}