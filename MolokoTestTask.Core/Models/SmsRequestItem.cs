﻿namespace MolokoTestTask.Core.Models
{
    public class SmsRequestItem
    {
        #region Properties

        public int Id { get; set; }

        public string Receiver { get; set; }

        public string Sender { get; set; }

        public string Message { get; set; }

        #endregion
    }
}