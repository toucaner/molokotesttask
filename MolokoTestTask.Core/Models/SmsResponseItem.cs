﻿namespace MolokoTestTask.Core.Models
{
    public class SmsResponseItem
    {
        #region Properties

        public int Id { get; set; }

        public int ServerId { get; set; }

        public string Message { get; set; }

        public string ErrorCode { get; set; }

        #endregion
    }
}